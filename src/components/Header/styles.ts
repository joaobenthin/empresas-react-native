import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/FontAwesome';

export const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 12px 16px;
`;

export const Title = styled.Text`
  color: #fff;
  font-size: 24px;
`;

export const WrapperButton = styled.View``;
