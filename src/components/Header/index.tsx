import React, { useState } from 'react';
import { TextInput, Button } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

import { WrapperButton } from './styles';

interface HeaderProps {
  onSearch: (searchText: string, enterpriseTypeName: string) => void;
  enterpriseTypes: EnterpriseType[];
}

interface EnterpriseType {
  id: number;
  enterprise_type_name: string;
}

const Header: React.FC<HeaderProps> = ({ onSearch, enterpriseTypes }) => {
  const [searchText, setSearchText] = useState('');
  const [enterpriseTypeSelect, setEnterpriseTypeSelect] = useState('');
  const enterpriseTypesItens = enterpriseTypes.map(
    (enterpriseTypesItem: EnterpriseType) => ({
      value: enterpriseTypesItem.id,
      label: enterpriseTypesItem.enterprise_type_name,
    }),
  );

  function changeSearchText(text: string) {
    setSearchText(text);
  }

  function changeEnterpriseType(enterpriseType: string) {
    setEnterpriseTypeSelect(enterpriseType);
  }

  function changeFilter() {
    onSearch(searchText, enterpriseTypeSelect);
  }

  return (
    <>
      <TextInput
        style={{ marginLeft: 8 }}
        value={searchText}
        onChangeText={changeSearchText}
        placeholder="Enter enterprise name"
      />
      <RNPickerSelect
        items={enterpriseTypesItens}
        onValueChange={changeEnterpriseType}
        placeholder={{ label: 'Select an type' }}
        value={enterpriseTypeSelect}
      />
      <WrapperButton>
        <Button title="Apply Filter" onPress={changeFilter} />
      </WrapperButton>
    </>
  );
};

export default Header;
