import React, { useEffect, useState } from 'react';
import { RouteProp } from '@react-navigation/native';

import { StackNavigationProp } from '@react-navigation/stack';
import api from '../../services/api';
import { Container, Image, Title, Description, Text } from './styles';
import { useAuth } from '../../hooks/auth';

type RootStackParamList = {
  EnterpriseDetails: { enterpriseId: string };
};

type EnterpriseDetailsScreenRouteProp = RouteProp<
  RootStackParamList,
  'EnterpriseDetails'
>;

type EnterpriseDetailsScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'EnterpriseDetails'
>;

type Props = {
  route: EnterpriseDetailsScreenRouteProp;
  navigation: EnterpriseDetailsScreenNavigationProp;
};

interface Enterprise {
  photo: string;
  enterprise_name: string;
  description: string;
  city: string;
  country: string;
}

const EnterpriseDetails: React.FC<Props> = ({ route }) => {
  const [enterprise, setEnterprise] = useState<Enterprise>();
  const { headers } = useAuth();

  const { enterpriseId } = route.params;

  useEffect(() => {
    async function loadEnterprise() {
      const response = await api.get(`enterprises/${enterpriseId}`, {
        headers: {
          'access-token': headers.accessToken,
          client: headers.client,
          uid: headers.uid,
        },
      });

      setEnterprise(response.data.enterprise);
    }

    loadEnterprise();
  }, [enterpriseId, headers]);

  if (!enterprise) {
    return null;
  }

  return (
    <Container>
      <Image
        style={{ width: 150, height: 150 }}
        source={{ uri: `https://empresas.ioasys.com.br${enterprise.photo}` }}
      />
      <Title>{enterprise.enterprise_name}</Title>
      <Description>{enterprise.description}</Description>
      <Text>
        {enterprise.city}
        {' - '}
        {enterprise.country}
      </Text>
    </Container>
  );
};

export default EnterpriseDetails;
