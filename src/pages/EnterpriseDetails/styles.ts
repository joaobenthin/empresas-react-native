import styled from 'styled-components/native';

export const Container = styled.View`
  align-items: center;
  padding-top: 16px;
`;

export const Image = styled.Image``;

export const Title = styled.Text`
  margin-top: 8px;
  font-size: 24px;
  font-weight: bold;
`;

export const Description = styled.Text`
  font-size: 12px;
  margin: 0 24px;
`;

export const Text = styled.Text`
  margin-top: 12px;
  font-size: 16px;
`;
