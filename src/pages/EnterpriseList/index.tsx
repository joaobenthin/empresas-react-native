import { useNavigation } from '@react-navigation/native';
import React, { useCallback, useEffect, useState } from 'react';
import { FlatList, Pressable, StyleSheet, Text } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import Header from '../../components/Header';
import { useAuth } from '../../hooks/auth';

import api from '../../services/api';

interface Enterprise {
  id: number;
  enterprise_name: string;
  enterprise_type: EnterpriseType;
}

interface EnterpriseType {
  id: number;
  enterprise_type_name: string;
}

interface LoadEnterprisesProps {
  searchText: string;
  enterpriseTypeName: string;
}

const EnterpriseList: React.FC = () => {
  const navigation = useNavigation();
  const [enterprises, setEnterprises] = useState<Enterprise[]>([]);
  const [enterpriseTypes, setEnterpriseTypes] = useState<EnterpriseType[]>([]);
  const { headers, signOut } = useAuth();

  const loadEnterprises = useCallback(
    async ({
      searchText,
      enterpriseTypeName,
    }: Partial<LoadEnterprisesProps>) => {
      try {
        const response = await api.get('enterprises', {
          params: { name: searchText, enterprise_types: enterpriseTypeName },
          headers: {
            'access-token': headers.accessToken,
            client: headers.client,
            uid: headers.uid,
          },
        });

        setEnterprises(response.data.enterprises);
      } catch (err) {
        if (err.response.status === 401) {
          signOut();
        }
      }
    },
    [],
  );

  useEffect(() => {
    loadEnterprises({});
  }, [loadEnterprises]);

  useEffect(() => {
    if (enterpriseTypes.length === 0) {
      const enterpriseTypesResponse = enterprises.map(
        (enterprise: Enterprise) => {
          return {
            id: enterprise.enterprise_type.id,
            enterprise_type_name:
              enterprise.enterprise_type.enterprise_type_name,
          };
        },
      );

      const enterpriseTypeUnique = [
        ...new Map(
          enterpriseTypesResponse.map(item => [item.id, item]),
        ).values(),
      ];

      setEnterpriseTypes(enterpriseTypeUnique);
    }
  }, [enterprises]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Header
        onSearch={(searchText: string, enterpriseTypeName: string) =>
          loadEnterprises({ searchText, enterpriseTypeName })}
        enterpriseTypes={enterpriseTypes}
      />
      <FlatList
        data={enterprises}
        style={{ backgroundColor: '#f5f5f5' }}
        renderItem={({ item }) => (
          <Pressable
            style={{
              paddingHorizontal: 16,
              paddingVertical: 24,
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderBottomColor: '#000',
            }}
            onPress={() =>
              navigation.navigate('EnterpriseDetails', {
                enterpriseId: item.id,
              })}
          >
            <Text style={{ color: '#212121' }} key={item.id}>
              {item.enterprise_name}
            </Text>
          </Pressable>
        )}
      />
    </SafeAreaView>
  );
};

export default EnterpriseList;
