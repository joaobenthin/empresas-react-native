import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import EnterpriseList from '../pages/EnterpriseList';
import EnterpriseDetails from '../pages/EnterpriseDetails';

const App = createStackNavigator();

const AppRoutes: React.FC = () => (
  <App.Navigator
    screenOptions={{
      cardStyle: { backgroundColor: '#e5e5e5' },
    }}
  >
    <App.Screen
      name="EnterpriseList"
      component={EnterpriseList}
      options={{ headerTitle: 'Enterprises' }}
    />
    <App.Screen
      name="EnterpriseDetails"
      component={EnterpriseDetails}
      options={{ headerTitle: 'Enterprise' }}
    />
  </App.Navigator>
);

export default AppRoutes;
