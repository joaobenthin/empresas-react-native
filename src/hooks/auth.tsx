import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../services/api';

interface AuthState {
  client: string;
  uid: string;
  accessToken: string;
}

interface SignInCredentials {
  email: string;
  password: string;
}

interface AuthContextData {
  headers: AuthState;
  loading: boolean;
  signIn(credentials: SignInCredentials): void;
  signOut(): void;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

const AuthProvider: React.FC<React.ReactNode> = ({ children }) => {
  const [headers, setHeaders] = useState<AuthState>({} as AuthState);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function loadStoragedData(): Promise<void> {
      const [client, uid, accessToken] = await AsyncStorage.multiGet([
        '@ioasys:client',
        '@ioasys:uid',
        '@ioasys:accessToken',
      ]);

      if (client[1] && uid[1] && accessToken[1]) {
        setHeaders({
          client: client[1],
          uid: uid[1],
          accessToken: accessToken[1],
        });
      }

      setLoading(false);
    }

    loadStoragedData();
  }, []);

  const signIn = useCallback(async ({ email, password }) => {
    const response = await api.post('users/auth/sign_in', {
      email,
      password,
    });

    const { client, uid } = response.headers;
    const accessToken = response.headers['access-token'];

    await AsyncStorage.multiSet([
      ['@ioasys:client', client],
      ['@ioasys:uid', uid],
      ['@ioasys:accessToken', accessToken],
    ]);

    setHeaders({ client, uid, accessToken });
  }, []);

  const signOut = useCallback(async () => {
    await AsyncStorage.multiRemove([
      '@ioasys:client',
      '@ioasys:uid',
      '@ioasys:accessToken',
    ]);

    setHeaders({} as AuthState);
  }, []);

  return (
    <AuthContext.Provider value={{ headers, loading, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth(): AuthContextData {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }

  return context;
}

export { AuthProvider, useAuth };
